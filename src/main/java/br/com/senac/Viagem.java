/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author sala304b
 */
public class Viagem {
    
    private final double consumo = 12;
    private final double tempo;
    private final double velocidade;
    private double distancia;
    private double litrosUsados;
    
    public Viagem(double tempoHora, double velocidadeHora){
        this.tempo = tempoHora;
        this.velocidade = velocidadeHora;
    }
    
    public double calculoDistancia(){
        this.distancia = this.tempo * this.velocidade;
        return this.distancia;
    }
    
    public double calculoLitros(){
        this.litrosUsados = calculoDistancia() / consumo;
        return this.litrosUsados;
    }

    
    
    
    
        
    
}
