/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class ViagemTest {
    
    public ViagemTest() {
    }
    
    @Test
    public void deveCalcularDistancia(){
        Viagem viagem = new Viagem(10, 80);
        assertEquals(800,viagem.calculoDistancia(), 0.1);
    }
    
    @Test
    public void deveCalcularConsumo(){
        Viagem viagem = new Viagem(10, 80);
       
        double resultado = viagem.calculoLitros()  ;
        assertEquals(66.66, resultado, 0.01);
    }
}
